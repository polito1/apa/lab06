/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/14/2020
 */

#include <stdio.h>
#include <stdlib.h>

void malloc2dP(int ***matrix, int *nr, int *nc);
void separa(int **mat, int nr, int nc, int *white, int *black);

int main() {
    int nr, nc, **matrix, *white, *black, dimWhite, dimBlack;

    // Richiamo la funzione per allocare la matrice.
    malloc2dP(&matrix, &nr, &nc);

    // Gestione degli errori.
    if (matrix == NULL) {
        printf("Error.");
        return -1;
    }

    // Se il numero di celle nella matrice è pari, allora la divisione tra bianchi e neri sarà perfetta,
    // Altrimenti i bianchi saranno uno più dei neri.
    if ((nr * nc) % 2 == 0) {
        dimWhite = dimBlack = (nr * nc)/2;
    } else {
        dimWhite = (nr * nc + 1)/2;
        dimBlack = dimWhite - 1;
    }

    // Alloco dinamicamente i vettori.
    white = (int*) malloc(dimWhite * sizeof(int));
    black = (int*) malloc(dimBlack * sizeof(int));

    // Richiamo la funzione per separare i bianchi dai neri.
    separa(matrix, nr, nc, white, black);

    // Stampo i valori.
    printf("====== white:\n");
    for (int i = 0; i < dimWhite; i++) {
        printf("%d ", white[i]);
    }

    printf("\n====== black:\n");
    for (int i = 0; i < dimBlack; i++) {
        printf("%d ", black[i]);
    }

    // Libero la memoria.
    free(white);
    free(black);

    return 0;
}

// Funzione per allocare e riempire la matrice.
void malloc2dP(int ***matrix, int *nr, int *nc)
{
    FILE *fp = fopen("mat.txt", "r");

    // Gestione degli errori legati al file.
    if (fp == NULL) {
        printf("File error.");
        matrix = NULL;
        return;
    }

    // Leggo le dimensioni della matrice.
    fscanf(fp, "%d %d", nr, nc);

    // Alloco dinamicamente una matrice di interi.
    int **m = malloc(*nr * sizeof(int*));

    for (int i = 0; i < *nr; i++) {
        m[i] = malloc(*nc * sizeof(int));
    }

    // Leggo i valori delle celle della matrice da file.
    for (int i = 0; i < *nr; i++) {
        for (int j = 0; j < *nc; j++) {
            fscanf(fp, "%d", &m[i][j]);
        }
    }

    // Chiudo il file: non ci è più utile.
    fclose(fp);

    // Assegno al valore del puntatore `matrix` il valore della matrice m.
    *matrix = m;
}

// Funzione che separa i neri dai bianchi e li inserisce nei vettori.
void separa(int **mat, int nr, int nc, int *white, int *black)
{
    int countWhite = 0, countBlack = 0, count = 0;

    for (int i = 0; i < nr; i++) {
        for (int j = 0; j < nc; j++) {
            // Se partiamo con la cella bianca, le posizioni pari e la 0 saranno bianche
            // (Nota: 0 % 2 ritorna 0).
            if (count % 2 == 0) {
                white[countWhite ++] = mat[i][j];
            } else {
                black[countBlack ++] = mat[i][j];
            }

            count ++;
        }
    }
}
