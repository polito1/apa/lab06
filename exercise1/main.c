/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/14/2020
 */


#include <stdio.h>
#include <stdlib.h>

// Massima lunghezza delle stringhe, definita nella consegna
#define MAX_STRING_LENGTH 255

// Definisco una struttua per l'entità `amico`.
typedef struct {
    int songsNumber;
    char **songs;
} friend;

// Definisco le funzioni che mi serviranno.
friend *readFile(char *fileName, int *length);
int rec(int pos, friend *friends, char **solution, int n, int count);
void deallocate(friend *friends, char **solution, int friendsNumber);

int main() {
    friend *friends;
    int friendsNumber, initPos = 0, count = 0;
    char **solution;

    friends = readFile("brani.txt", &friendsNumber);

    if (friends == NULL) {
        printf("Error.");
        return -1;
    }

    // Alloco dinamicamente un vettore di stringhe.
    solution = (char **) malloc(friendsNumber * sizeof(char *));

    // Alloco dinamicamente una stringa (in C: un vettore di chars).
    for (int i = 0; i < friendsNumber; i++) {
        solution[i] = (char *) malloc(MAX_STRING_LENGTH * sizeof(char *));
    }

    // Count rappresenta il numero di soluzioni trovate, cioè la moltiplicatoria della cardinalità di ogni
    // insieme.
    // Nel nostro caso rappresenta la moltiplicatoria del numero di canzoni di ogni amico.
    count = rec(initPos, friends, solution, friendsNumber, count);

    printf("%d Solutions found.", count);

    // Richiamo la funzione per pulire la memoria.
    deallocate(friends, solution, friendsNumber);

    return 0;
}

friend *readFile(char *fileName, int *length)
{
    friend *friends;
    int songsNumber = 0;
    FILE *fp = fopen(fileName, "r");

    // Gestione dell'errore sul file.
    if (fp == NULL) {
        printf("File error.");
        return NULL;
    }

    fscanf(fp, "%d", length);

    // Alloco dinamicamente un vettore di struct.
    friends = (friend *) malloc(*length * sizeof(friend));

    for (int i = 0; i < *length; i++) {
        fscanf(fp, "%d", &songsNumber);
        friends[i].songsNumber = songsNumber;
        // Alloco dinamicamente un vettore di stringhe.
        friends[i].songs = (char **) malloc(songsNumber * sizeof(char *));

        for (int j = 0; j < songsNumber; j++) {
            // Alloco dinamicamente una stringa (in C: un vettore di chars).
            friends[i].songs[j] = (char *) malloc(MAX_STRING_LENGTH * sizeof(char *));

            fscanf(fp, "%s", friends[i].songs[j]);
        }
    }

    // Chiudo il dile
    fclose(fp);

    return friends;
}

int rec(int pos, friend *friends, char **solution, int n, int count)
{
    // Condizione di terminazione.
    if (pos >= n) {
        for (int i = 0; i < n; i++) {
            printf("%s\n", solution[i]);
        }

        printf("\n");
        // Ritorno il numero di soluzioni aggiornato.
        return ++count;
    }

    // Effettuo una scelta e richiamo la funzione ricorsivamente.
    for (int i = 0; i < friends[pos].songsNumber; i++) {
        solution[pos] = friends[pos].songs[i];
        count = rec(pos + 1, friends, solution, n, count);
    }

    // Ritorno il numero di soluzioni aggiornato.
    return count;
}

void deallocate(friend *friends, char **solution, int friendsNumber)
{
    // Libero la memoria dedicata alla soluzione.
    free(solution);

    // Libero la memoria dedicata ad ogni canzone di ogni amico.
    for (int i = 0; i < friendsNumber; i++) {
        for (int j = 0; j < friends[i].songsNumber; j++) {
            free(friends[i].songs[j]);
        }

        free(friends[i].songs);
    }

    // Libero la memoria dedicata agli amici.
    free(friends);
}
