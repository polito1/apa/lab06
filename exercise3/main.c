/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/14/2020
 */

#include <stdio.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAX_COMMAND_LENGTH 15
#define MAX_STRING_LENGTH 30

// Definisco una struct contenente le informazioni sulla corsa.
typedef struct {
    char codice_tratta[MAX_STRING_LENGTH + 1];
    char partenza[MAX_STRING_LENGTH + 1];
    char destinazione[MAX_STRING_LENGTH + 1];
    char data[MAX_STRING_LENGTH + 1];
    char ora_partenza[MAX_STRING_LENGTH + 1];
    char ora_arrivo[MAX_STRING_LENGTH + 1];
    int ritardo;
} corsa;

// Definisco un tipo enum per i comandi.
typedef enum {
    stampa_video,
    stampa_file,
    ordina_data,
    ordina_tratta,
    ordina_partenza,
    ordina_arrivo,
    trova_partenza,
    carica_file,
    r_fine
} comando_e;

// Prototipi di tutte le funzioni che andrò ad utilizzare.
void printCorsa(corsa value);
comando_e leggiComando();
void strToLower(char str [MAX_STRING_LENGTH + 1]);
void stampaFile(corsa *corse, int n);
void ordinaData(corsa **corse, int n);
void ordinaTratta(corsa **corse, int n);
void ordinaPartenza(corsa **corse, int n);
void ordinaArrivo(corsa **corse, int n);
void trovaPartenzaDicotomica(corsa **corse, int n);
void partenzaDicotomica(corsa **corse, int n, char *partenza);
void mergeSort(corsa **corse, int n, int (*compareCorsa) (corsa a, corsa b));
void mergeSortRecursive(corsa **corse, corsa **corseB, int l, int r, int (*compareCorsa) (corsa a, corsa b));
void merge(corsa **corse, corsa **corseB, int l, int middle, int r, int (*compareCorsa) (corsa a, corsa b));
void printArray(corsa *corse, int n);
void printArrayOfPointers(corsa **corse, int n);
int readFile(corsa **original, corsa ***orderByData, corsa ***orderByTratta, corsa ***orderByPartenza, corsa ***orderByArrivo, int firstTime);
void allocate(corsa **original, corsa ***orderByData, corsa ***orderByTratta, corsa ***orderByPartenza, corsa ***orderByArrivo, int structNumber);
void deallocate(corsa *original, corsa **orderByData, corsa **orderByTratta, corsa **orderByPartenza, corsa **orderByArrivo);
void ordinaTotale(corsa **data, corsa **tratta, corsa **partenza, corsa **arrivo, int n);

//Funzioni per il confronto delle corse secondo i vari criteri
int compareData(corsa a, corsa b);
int compareTratta(corsa a, corsa b);
int comparePartenza(corsa a, corsa b);
int compareArrivo(corsa a, corsa b);


int main() {
    int n, continua = 1;
    comando_e comando;
    corsa *original, **orderByData, **orderByTratta, **orderByPartenza, **orderByArrivo;

    // Leggo il file (implica la sola allocazione dei vettori).
    n = readFile(&original, &orderByData, &orderByTratta, &orderByPartenza, &orderByArrivo, 1);

    // Gestione dell'errore sul file.
    if (n == -1) {
        printf("Error.");
        return -1;
    }

    // Riordino i vettori.
    ordinaTotale(orderByData, orderByTratta, orderByPartenza, orderByArrivo, n);

    // Finchè non arriva un comando sconosciuto oppure il comando `fine`
    while (continua) {
        comando = leggiComando();

        // Richiama la funzione adeguata a seconda del comando.
        switch (comando) {
            case r_fine:
                continua = 0;
                break;
            case stampa_video:
                printArray(original, n);
                break;
            case stampa_file:
                stampaFile(original, n);
                break;
            case ordina_data:
                printArrayOfPointers(orderByData, n);
                break;
            case ordina_tratta:
                printArrayOfPointers(orderByTratta, n);
                break;
            case ordina_partenza:
                printArrayOfPointers(orderByPartenza, n);
                break;
            case ordina_arrivo:
                printArrayOfPointers(orderByArrivo, n);
                break;
            case trova_partenza:
                trovaPartenzaDicotomica(orderByPartenza, n);
                break;
            case carica_file:
                // Leggo il file (implica la deallocazione e riallocazione dei vettori)
                n = readFile(&original, &orderByData, &orderByTratta, &orderByPartenza, &orderByArrivo, 0);

                // Gestione dell'errore sul file.
                if (n == -1) {
                    printf("Error.");
                    return -1;
                }

                // Riordino i vettori.
                ordinaTotale(orderByData, orderByTratta, orderByPartenza, orderByArrivo, n);
                break;
            default:
                continua = 0;
                break;
        }
    }

    return 0;
}

// Stampa la corsa formattata a dovere.
void printCorsa(corsa value)
{
    printf(
            "Codice: %s, Partenza: %s, Destinazione: %s, Data: %s, Ora partenza: %s, Ora arrivo: %s, Ritardo: %d;\n",
            value.codice_tratta,
            value.partenza,
            value.destinazione,
            value.data,
            value.ora_partenza,
            value.ora_arrivo,
            value.ritardo
    );
}

/*
 * Questa funzione legge il comando passato come input; grazie al tipo enum, possiamo utilizzare lo switch.
 */
comando_e leggiComando()
{
    comando_e c;
    char command[MAX_COMMAND_LENGTH + 1];
    char tabella[r_fine + 1][MAX_COMMAND_LENGTH + 1] = {
            "stampa_video", "stampa_file", "ordina_data", "ordina_tratta", "ordina_partenza", "ordina_arrivo", "trova_partenza", "carica_file", "fine"
    };

    printf("Comando (stampa_video, stampa_file, ordina_data, ordina_tratta, ordina_partenza, ordina_arrivo, trova_partenza, carica_file, fine): ");
    scanf("%s", command);

    strToLower(command);

    c = stampa_video;

    while (c < r_fine && strcmp(command, tabella[c]) != 0) {
        c++;
    }

    return c;
}

// Sostituisce ogni carattere della stringa con il suo corrispondente in lowercase.
void strToLower(char str [MAX_STRING_LENGTH + 1])
{
    for (int i = 0; i < strlen(str); i++) {
        str[i] = (char) tolower(str[i]);
    }
}

// Stampa un vettore di corse formattato a dovere.
void printArray(corsa *corse, int n)
{
    printf("==========\n");
    for (int i = 0; i < n; i++) {
        printCorsa(corse[i]);
    }

    printf("==========\n");
}

// Stampa un vettore di puntatori a corse formattato a dovere.
void printArrayOfPointers(corsa **corse, int n) {
    printf("==========\n");
    for (int i = 0; i < n; i++) {
        printCorsa(*corse[i]);
    }

    printf("==========\n");
}

// Stampa su file il vettore di corse.
void stampaFile(corsa *corse, int n)
{
    FILE *fo = fopen("output.txt", "w");

    if (fo == NULL) {
        return;
    }

    for (int i = 0; i < n; i++) {
        fprintf(
                fo,
                "%s %s %s %s %s %s %d\n",
                corse[i].codice_tratta,
                corse[i].partenza,
                corse[i].destinazione,
                corse[i].data,
                corse[i].ora_partenza,
                corse[i].ora_arrivo,
                corse[i].ritardo
        );
    }

    fclose(fo);
}

void ordinaData(corsa **corse, int n)
{
    mergeSort(corse, n, compareData);
}

void ordinaTratta(corsa **corse, int n)
{
    mergeSort(corse, n, compareTratta);
}

void ordinaPartenza(corsa **corse, int n)
{
    mergeSort(corse, n, comparePartenza);
}

void ordinaArrivo(corsa **corse, int n)
{
    mergeSort(corse, n, compareArrivo);
}

void partenzaDicotomica(corsa **corse, int n, char *partenza)
{
    int l = 0, r = n - 1, m, continua = 1, diff, tmp;

    while (l <= r && continua) {
        m = (l + r)/2;
        diff = corse[m]->partenza[0] - partenza[0];

        if (diff == 0) {
            if (strstr(corse[m]->partenza, partenza) != NULL) {
                printCorsa(*corse[m]);
                tmp = m - 1;
                while (tmp >= l && continua) {
                    if (strstr(corse[tmp]->partenza, partenza) != NULL) {
                        printCorsa(*corse[tmp]);
                    } else {
                        continua = 0;
                    }
                    tmp --;
                }
                tmp = m + 1;
                continua = 1;
                while (tmp <= r && continua) {
                    if (strstr(corse[tmp]->partenza, partenza) != NULL) {
                        printCorsa(*corse[tmp]);
                    } else {
                        continua = 0;
                    }
                    tmp ++;
                }
            }
            continua = 0;
        }

        if (diff < 0) {
            l = m + 1;
        } else {
            r = m - 1;
        }
    }
}

// Trova la partenza richiesta tramite un algoritmo dicotomico, più raffinato e con minor complessità.
void trovaPartenzaDicotomica(corsa **corse, int n)
{
    char partenza[MAX_STRING_LENGTH + 1];

    printf("Inserire la stazione di partenza da cercare: ");
    scanf("%s", partenza);

    partenzaDicotomica(corse, n, partenza);
}

// Algoritmo di ordinamento di complessità linearitmica, non in loco ma stabile (a noi interessa la stabilità).
void mergeSort(corsa **corse, int n, int (*compareCorsa) (corsa a, corsa b))
{
    int l = 0, r = n - 1;
    corsa **corseB = (corsa **) malloc(n * sizeof(corsa *));

    mergeSortRecursive(corse, corseB, l, r, compareCorsa);
}

// Porzione ricorsiva dell'algoritmo
void mergeSortRecursive(corsa **corse, corsa **corseB, int l, int r, int (*compareCorsa) (corsa a, corsa b))
{
    int middle;

    if (r <= l) {
        return;
    }

    middle = (l + r)/2;

    mergeSortRecursive(corse, corseB, l, middle, compareCorsa);
    mergeSortRecursive(corse, corseB, middle + 1, r, compareCorsa);
    merge(corse, corseB, l, middle, r, compareCorsa);
}

// Porzione di ricombinazione dell'algoritmo.
void merge(corsa **corse, corsa **corseB, int l, int middle, int r, int (*compareCorsa) (corsa a, corsa b))
{
    int i = l, j = middle + 1;

    for (int k = l; k <= r; k++) {
        if (i > middle) {
            corseB[k] = corse[j++];
        } else if (j > r) {
            corseB[k] = corse[i++];
        } else if (compareCorsa(*corse[i], *corse[j]) <= 0) {
            corseB[k] = corse[i++];
        } else {
            corseB[k] = corse[j++];
        }
    }

    for (int k = l; k <= r; k++) {
        corse[k] = corseB[k];
    }
}

// Funzione che confronta due corse basandosi sulla data.
int compareData(corsa a, corsa b)
{
    int res = strcmp(a.data, b.data);

    if (res == 0) {
        return strcmp(a.ora_partenza, b.ora_partenza);
    }

    return res;
}

// Funzione che confronta due corse basandosi sulla tratta.
int compareTratta(corsa a, corsa b)
{
    return strcmp(a.codice_tratta, b.codice_tratta);
}

// Funzione che confronta due corse basandosi sulla partenza.
int comparePartenza(corsa a, corsa b)
{
    return strcmp(a.partenza, b.partenza);
}

// Funzione che confronta due corse basandosi sull'arrivo.
int compareArrivo(corsa a, corsa b)
{
    return strcmp(a.destinazione, b.destinazione);
}

// Si occupa di deallocare/allocare i vettori necessari e riempirli con i valori letti da file.
// Ritorna il numero di corse lette.
int readFile(corsa **original, corsa ***orderByData, corsa ***orderByTratta, corsa ***orderByPartenza, corsa ***orderByArrivo, int firstTime)
{
    int size;
    char fileName[MAX_STRING_LENGTH + 1];
    FILE *fp;

    printf("Inserire il nome del file (max %d caratteri): ", MAX_STRING_LENGTH);
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    if (fp == NULL) {
        return -1;
    }

    fscanf(fp, "%d", &size);

    // Se non è la prima volta che utilizziamo la funzione, i vettori saranno già presenti in memoria, quindi
    // vanno deallocati.
    if (!firstTime) {
        deallocate(*original, *orderByData, *orderByTratta, *orderByPartenza, *orderByArrivo);
    }

    // Allocazione dinamica dei vettori.
    allocate(original, orderByData, orderByTratta, orderByPartenza, orderByArrivo, size);

    for (int i = 0; i < size; i++) {
        // Leggo da file una corsa e la inserisco nel vettore originale.
        fscanf(fp, "%s %s %s %s %s %s %d",
           (*original)[i].codice_tratta,
           (*original)[i].partenza,
           (*original)[i].destinazione,
           (*original)[i].data,
           (*original)[i].ora_partenza,
           (*original)[i].ora_arrivo,
           &(*original)[i].ritardo
        );

        // Inserisco in ogni vettore dei criteri di ordinamento un puntatore alla
        // struct appena inserita nel vettore originale.
        (*orderByData)[i] = &(*original)[i];
        (*orderByTratta)[i] = &(*original)[i];
        (*orderByPartenza)[i] = &(*original)[i];
        (*orderByArrivo)[i] = &(*original)[i];
    }

    // Ritorno il numero di struct lette.
    return size;
}

// Si occupa di allocare i vettori passati come parametri.
void allocate(corsa **original, corsa ***orderByData, corsa ***orderByTratta, corsa ***orderByPartenza, corsa ***orderByArrivo, int structNumber)
{
    *original = (corsa *) malloc(structNumber * sizeof(corsa));
    *orderByData = (corsa **) malloc(structNumber * sizeof(corsa *));
    *orderByTratta = (corsa **) malloc(structNumber * sizeof(corsa *));
    *orderByPartenza = (corsa **) malloc(structNumber * sizeof(corsa *));
    *orderByArrivo = (corsa **) malloc(structNumber * sizeof(corsa *));
}

// Si occupa di liberare i vettori passati come parametri.
void deallocate(corsa *original, corsa **orderByData, corsa **orderByTratta, corsa **orderByPartenza, corsa **orderByArrivo)
{
    free(orderByData);
    free(orderByTratta);
    free(orderByPartenza);
    free(orderByArrivo);
    free(original);
}

// Riordina i vettori secondo i vari criteri.
void ordinaTotale(corsa **data, corsa **tratta, corsa **partenza, corsa **arrivo, int n)
{
    ordinaData(data, n);
    ordinaTratta(tratta, n);
    ordinaPartenza(partenza, n);
    ordinaArrivo(arrivo, n);
}
